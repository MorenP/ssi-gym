import {Component, OnInit} from '@angular/core';
import {ActivitiesService} from "../../shared/service/activities.service";
import {ActivityType} from "../../shared/model/interface/activity-type.interface";

@Component({
  selector: 'activities',
  templateUrl: './activities.component.html',
  styles: [require("./../../assets/css/gym.css").toString()],
  providers: [ActivitiesService]
})
export class ActivitiesComponent implements OnInit{

  activityTypes: ActivityType[];
  selectedActivityType: ActivityType = null;

  constructor(private activitiesService: ActivitiesService) {
  }

  ngOnInit(): void {
    this.activitiesService.getActivityTypes().subscribe(activityTypes => this.activityTypes = activityTypes);
  }

  selectActivity(activityType: ActivityType) {

    this.selectedActivityType = activityType;

    for(let factivityType of this.activityTypes) {
      if(factivityType.activityTypeId !== activityType.activityTypeId) {
        factivityType.selected = false;
      }
    }
    activityType.selected = true;
  }

  closeActivityType(activityType: ActivityType) {
    this.selectedActivityType.selected = false;
    activityType.selected = false;
  }
}
