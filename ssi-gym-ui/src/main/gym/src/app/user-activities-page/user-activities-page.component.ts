import {Component, Input, OnInit} from '@angular/core';
import {Page} from "../../shared/model/class/page.class";

@Component({
  selector: 'user-activities-page',
  templateUrl: './user-activities-page.component.html',
  styles: [],
  providers: []
})
export class UserActivitiesPageComponent implements OnInit {

  @Input() page: Page;

  constructor() {
  }

  ngOnInit(): void {
  }

}
