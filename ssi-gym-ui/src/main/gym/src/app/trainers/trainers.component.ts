import {Component, OnInit} from '@angular/core';
import {TrainersService} from "../../shared/service/trainers.service";
import {Trainer} from "../../shared/model/interface/trainer.interface";

@Component({
  selector: 'trainers',
  templateUrl: './trainers.component.html',
  styles: [],
  providers: [TrainersService]
})
export class TrainersComponent implements OnInit {

  trainers: Trainer[];

  constructor(private trainersService: TrainersService) {
  }

  ngOnInit(): void {
    this.trainersService.getTrainers().subscribe(trainers => this.trainers = trainers);
  }

  getPicturePath(trainer: Trainer): string {
    let picturePath = "assets/img/" + trainer.pictureName;
    return picturePath;
  }
}
