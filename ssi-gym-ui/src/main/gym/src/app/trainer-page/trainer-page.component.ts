import {Component, Input, OnInit} from '@angular/core';
import {Page} from "../../shared/model/class/page.class";

@Component({
  selector: 'trainer-page',
  templateUrl: './trainer-page.component.html',
  styles: [],
  providers: []
})
export class TrainerPageComponent implements OnInit {

  @Input() page: Page;

  constructor() {
  }

  ngOnInit(): void {
  }
}
