import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {Routes} from "@angular/router";
import {AppComponent} from './app.component';
import {ActivitiesComponent} from "./activities/activities.component";
import {MainPageComponent} from "./main-page/main-page.component";
import {PriceListComponent} from "./price-list/price-list.component";
import {TrainersComponent} from "./trainers/trainers.component";
import {ContactComponent} from "./contatct/contact.component";
import {HttpModule} from "@angular/http";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {RegistrationComponent} from "./registration-page/registration-page.component";
import {LoginComponent} from "./login-page/login-page.component";
import {HttpClientModule} from "@angular/common/http";
import {UserPageComponent} from "./user-page/user-page.component";
import {TrainerPageComponent} from "./trainer-page/trainer-page.component";
import {CalendarComponent} from "./universal-components/calendar-component/calendar.component";
import {UserActivitiesPageComponent} from "./user-activities-page/user-activities-page.component";
import {UserAccountDetailsPageComponent} from "./user-page/user-account/user-account-details-page.component";


const appRoutes: Routes = [
  {path: 'index', component: AppComponent},
  {
    path: '',
    redirectTo: 'index',
    pathMatch: 'full'
  }
]

@NgModule({
  declarations: [
    AppComponent,
    ActivitiesComponent,
    MainPageComponent,
    PriceListComponent,
    TrainersComponent,
    ContactComponent,
    RegistrationComponent,
    LoginComponent,
    UserPageComponent,
    TrainerPageComponent,
    CalendarComponent,
    UserActivitiesPageComponent,
    UserAccountDetailsPageComponent
  ],
  imports: [BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
