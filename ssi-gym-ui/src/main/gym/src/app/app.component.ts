import {Component} from '@angular/core';
import {Page} from "../shared/model/class/page.class";
import {Md5} from "ts-md5/dist/md5";
import {User} from "../shared/model/class/user.class";
import {UserService} from "../shared/service/user.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styles: [require("./../assets/css/gym.css").toString(),
    require("./../assets/css/styl.css").toString(),
    require("./../assets/css/custom.css").toString(),
  ],
  providers: [UserService]
})
export class AppComponent {

  page: Page;
  user: User;

  constructor(private userService: UserService) {
    this.page = new Page();
    this.setPage("user_activities_page");
  }

  public setPage(name: string) {
    this.page.setPage(name);
  }

  public userUpdate(user: User) {
    this.user = user;
  }

  public logOut() {
    this.user = null;
    this.setPage("main_page");
  }

  public login() {
    this.userService.getUser(null).subscribe(user => {
      this.user = user;
      this.setPage("user_account_details_page");
    });
  }
}
