import {Component, Input, OnInit} from '@angular/core';
import {Page} from "../../../shared/model/class/page.class";
import {User} from "../../../shared/model/class/user.class";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'user-account-details-page',
  templateUrl: './user-account-details-page.component.html',
  styles: [],
  providers: []
})
export class UserAccountDetailsPageComponent implements OnInit {

  @Input() page: Page;
  @Input() user: User;

  userForm: FormGroup;
  tempUser: User;

  editMode: boolean;

  constructor(public fb: FormBuilder) {
  }

  ngOnInit(): void {
    this.editMode = false;
    this.tempUser = User.createNewUser(this.user);
    this.userForm = this.fb.group({
      name: [this.tempUser.name, Validators.required],
      lastName: [this.tempUser.lastName, Validators.required],
      email: [this.tempUser.email, Validators.email],
      address: [this.tempUser.address, Validators.required],
      age: [this.tempUser.age, Validators.required]
    })

    this.createForm();
  }

  public setPage(name: string) {
    this.page.setPage(name);
  }

  public editData() {
    this.editMode = true;
    this.userForm.get('name').enable();
    this.userForm.get('lastName').enable();
    this.userForm.get('address').enable();
    this.userForm.get('age').enable();
  }

  private createForm() {
    this.userForm = new FormGroup({
      name: new FormControl({value: this.tempUser.name, disabled: !this.editMode}, Validators.required),
      lastName: new FormControl({value: this.tempUser.lastName, disabled: !this.editMode}, Validators.required),
      email: new FormControl({value: this.tempUser.email, disabled: !this.editMode}, Validators.email),
      address: new FormControl({value: this.tempUser.address, disabled: !this.editMode}, Validators.required),
      age: new FormControl({value: this.tempUser.age, disabled: !this.editMode}, Validators.required),
    });
  }

  public saveData() {
    this.editMode = false;
    this.userForm.get('name').disable();
    this.userForm.get('lastName').disable();
    this.userForm.get('address').disable();
    this.userForm.get('age').disable();
  }
}
