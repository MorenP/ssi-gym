import {Component, Input, OnInit} from '@angular/core';
import {Page} from "../../shared/model/class/page.class";
import {User} from "../../shared/model/class/user.class";

@Component({
  selector: 'user-page',
  templateUrl: './user-page.component.html',
  styles: [],
  providers: []
})
export class UserPageComponent implements OnInit {

  @Input() page: Page;
  @Input() user: User;

  constructor() {
  }

  ngOnInit(): void {
  }

  public setPage(name: string) {
    console.log(name)
    this.page.setPage(name);
  }
}
