import {Component, OnInit} from '@angular/core';
import {PassTypeService} from "../../shared/service/passType.service";
import {PassType} from "../../shared/model/class/passType.class";

@Component({
  selector: 'price-list',
  templateUrl: './price-list.component.html',
  styles: [],
  providers: [PassTypeService]
})
export class PriceListComponent implements OnInit {

  private passType: PassType;
  passTypes: PassType[];

  constructor(private passTypeService: PassTypeService) {
    this.passType = new PassType();
  }

  ngOnInit(): void {
    this.passTypeService.getAllPassTypes().subscribe(passTypes => this.passTypes = passTypes);
  }
}
