import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {Page} from "../../shared/model/class/page.class";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Login} from "../../shared/model/class/login.class";
import {UserService} from "../../shared/service/user.service";
import {User} from "../../shared/model/class/user.class";


@Component({
  selector: 'login-page',
  templateUrl: './login-page.component.html',
  styles: [],
  providers: [UserService]
})
export class LoginComponent implements OnInit{

  @Input() page: Page;
  @Input() user: User;
  @Output() userUpdate = new EventEmitter();

  loginClass: Login;
  loginForm: FormGroup;

  userLoginError: boolean = false;
  loginAction: boolean = false;

  ngOnInit(): void {
    this.loginAction = false;
  }

  constructor(private fb: FormBuilder, private userService: UserService) {
    this.loginClass = new Login();
    this.loginForm = this.fb.group({
      login: [this.loginClass.login, Validators.required],
      password: [this.loginClass.password, Validators.required],
    })
  }

  public login() {
    this.loginAction = true;
    this.userService.getUser(this.loginClass).subscribe(user => {
      if(!user.error) {
        this.userLoginError = false;
        this.user = user;
        this.userUpdate.emit(user);
        this.page.setUserPage();
      } else {
        this.userLoginError = true;
        this.loginAction = false;
      }
    });
  }

  public setPage(name: string) {
    this.page.setPage(name);
  }
}
