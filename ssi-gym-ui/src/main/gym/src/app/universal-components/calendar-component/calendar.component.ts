import {Component, OnInit} from '@angular/core';
import * as moment from "moment";

@Component({
  selector: 'calendar-component',
  templateUrl: './calendar.component.html',
  styles: [require("./calendar.component.css").toString()],
  providers: []
})
export class CalendarComponent implements OnInit {

  actualMonth: moment.Moment;
  actualTime: moment.Moment;
  actualMonth2: moment.Moment;

  weeks: number[];
  days: any[] = new Array(42);

  constructor() {
  }

  ngOnInit(): void {

    this.actualTime = moment();
    this.actualMonth = moment();
    this.actualMonth2 = moment();

    this.actualMonth = this.actualMonth.subtract(Number(this.actualMonth.format('D').toString())-1, 'days');
    this.actualMonth2 = this.actualMonth2.subtract(Number(this.actualMonth2.format('D').toString())-1, 'days');
    this.prepare(this.actualMonth);
  }

  getHeader(): string {
    return this.actualMonth.format("MMMM YYYY").toString();
  }

  getDay(x: number) {
    console.log(this.days[x].format("DD MM YYYY"));
  }

  previousMonth() {
    this.actualMonth.subtract(1, 'months');
    this.prepare(this.actualMonth);
  }

  nextMonth() {
    this.actualMonth.add(1, 'months');
    this.prepare(this.actualMonth);
  }

  getDayToDisplay(index: number): string {
    if(this.days[index]!== null) {
      return this.days[index].format('D').toString();
    } else {
      return "";
    }
  }

  prepare(actualMonth: moment.Moment) {
    this.weeks = [0,1,2,3,4];
    actualMonth = actualMonth.subtract(Number(actualMonth.format('D').toString())-1, 'days');
    let start: boolean = false;
    let index = 0;
    let amountOfDaysInMonth = actualMonth.daysInMonth();
    console.log("B: ", actualMonth.day());

    for (let i=1;i<amountOfDaysInMonth;i++) {

      if(!start) {
        for(let j=1;j<this.convertFormatDay(Number(actualMonth.day()));j++) {
          this.days[index]=null;
          index++;
        }
        this.days[index]=moment(actualMonth);
        index++;
        start = true;
      }
      this.days[index] = moment(actualMonth.add(1,'d'));
      index++;
    }

    for(index; index <42 ;index++) {
      this.days[index]=null;
    }
    console.log(this.days);

    for(let i = 35;i<42;i++) {
      if(this.days[i]) {
        this.weeks.push(5);
       break;
      }
    }
  }

  private convertFormatDay(x: number): number {
    if(x == 0) {
      return 7;
    } else {
      return x;
    }
  }
}


