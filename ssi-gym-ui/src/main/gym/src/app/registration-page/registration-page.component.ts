import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Page} from "../../shared/model/class/page.class";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Registration} from "../../shared/model/class/registration.class";
import {UserService} from "../../shared/service/user.service";
import {User} from "../../shared/model/class/user.class";

@Component({
  selector: 'registration-page',
  templateUrl: './registration-page.component.html',
  styles: [],
  providers: [UserService]
})
export class RegistrationComponent implements OnInit {

  @Input() page: Page;
  @Input() user: User;
  @Output() userUpdate = new EventEmitter();

  registrationClass: Registration;
  registrationForm: FormGroup;

  userRegistrationError: boolean = false;
  registrationAction: boolean = false;

  ngOnInit(): void {
    this.registrationAction = false;
  }

  constructor(public fb: FormBuilder, private userService: UserService){

    this.registrationClass = new Registration();
    this.registrationForm = this.fb.group({
      name: [this.registrationClass.name, Validators.required],
      lastName: [this.registrationClass.lastName, Validators.required],
      email: [this.registrationClass.email, Validators.email],
      password: [this.registrationClass.password, Validators.minLength(8)],
      repeatPassword: [this.registrationClass.repeatPassword, Validators.minLength(8)],
      address: [this.registrationClass.address, Validators.required],
    })
  }

  public setPage(name: string) {
    this.page.setPage(name);
  }

  public register() {
    this.registrationAction = true;
    // console.log(this.registrationClass.getJSON());
      this.userService.register(this.registrationClass).subscribe(user => {
        console.log(user);
        if(!user.error) {
          this.userRegistrationError = false;
          this.user = user;
          this.userUpdate.emit(user);
          this.page.setUserPage();
        } else {
          this.userRegistrationError = true;
          this.registrationAction = false;
        }
      });
    }
}
