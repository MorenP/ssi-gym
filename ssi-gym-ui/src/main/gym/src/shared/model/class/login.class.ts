import {Md5} from "ts-md5/dist/md5";

export class Login {
  login: string;
  password: string;

  constructor() {
    this.init();
    this.testData();
  }

  init() {
    this.login = "";
    this.password ="";
  }

  testData() {
    this.login ="mnowak@gmail.com";
    this.password ="qwertyuiop";
  }

  getPasswordHash(password: string): string {
    return Md5.hashStr(password).toString();
  }

  getJSON(): string{
    this.password = this.getPasswordHash(this.password);
    return JSON.stringify(this);
  }
}
