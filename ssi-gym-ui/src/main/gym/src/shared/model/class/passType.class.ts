import {IPassType} from "../interface/passType.interface";

export class PassType implements IPassType {
  passTypeId: number;
  name: string;
  price: number;
  description: string;

  constructor() {
    this.init();
    this.testData();
  }

  init() {
    this.passTypeId = 0;
    this.name = "";
    this.price = 0;
    this.description = "";
  }

  testData() {
    this.passTypeId = 10;
    this.name = "HALF";
    this.price = 59.0;
    this.description = "LOREM IPSUM";
  }

  getJSON(): string{
    let iPassType: IPassType = {
      passTypeId: this.passTypeId,
      name: this.name,
      price: this.price,
      description: this.description
    };
    return JSON.stringify(iPassType);
  }
}
