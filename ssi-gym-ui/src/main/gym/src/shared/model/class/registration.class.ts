import {Md5} from "ts-md5/dist/md5";
import {IRegistration} from "../interface/registration.interface";

export class Registration implements IRegistration{

  name: string;
  lastName: string;
  email: string;
  password: string;
  repeatPassword: string;
  address: string;

  constructor() {
    this.init();
    this.testData();
  }

  init() {
    this.name = "";
    this.lastName ="";
    this.email ="";
    this.password ="";
    this.repeatPassword ="";
    this.address = "";
  }

  testData() {
    this.name = "Andrzej";
    this.lastName ="Kowalski";
    this.email ="aakowalski@gmail.com";
    this.password ="qwertyuiop";
    this.repeatPassword ="qwertyuiop";
    this.address = "Kraków, Polna 23";
  }

  getPasswordHash(password: string): string {
    return Md5.hashStr(password).toString();
  }

  getJSON(): string{
    this.password = this.getPasswordHash(this.password);
    this.repeatPassword = this.getPasswordHash(this.repeatPassword);

    let iRegistration: IRegistration = {
      name: this.name,
      lastName: this.lastName,
      email: this.email,
      password: this.password,
      repeatPassword: this.repeatPassword,
      address: this.address
    };
    return JSON.stringify(iRegistration);
  }
}
