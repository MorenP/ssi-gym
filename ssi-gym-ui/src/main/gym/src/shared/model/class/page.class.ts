export class Page {

  public static USER_ACCOUNT_DETAILS: string = "user_account_details";

  name: string;
  mainPage: boolean;
  loginPage: boolean;
  registationPage: boolean;
  userPage: boolean;
  trainerPage: boolean;
  userActivitiesPage: boolean;
  userAccountDetailsPage: boolean;

  constructor() {
    this.setUserActivitiesPage();
  }

  setMainPage() {
    this.setAllFalse();
    this.mainPage = true;
  }

  setRegistrationPage() {
    this.setAllFalse();
    this.registationPage = true;
  }

  setLoginPage() {
    this.setAllFalse();
    this.loginPage = true;
  }

  setUserPage() {
    this.setAllFalse();
    this.userPage = true;
  }

  setTrainerPage() {
    this.setAllFalse();
    this.trainerPage = true;
  }

  setUserActivitiesPage() {
    this.setAllFalse();
    this.userActivitiesPage = true;
  }

  setUserAccountDetailsPage() {
    this.setAllFalse();
    this.userAccountDetailsPage = true;
  }

  private setAllFalse() {
    this.mainPage = false;
    this.registationPage = false;
    this.loginPage = false;
    this.userPage = false;
    this.trainerPage = false;
    this.userActivitiesPage = false;
    this.userAccountDetailsPage = false;
  }

  setPage(name: string) {
    switch(name) {
      case "main_page": {
        this.setMainPage();
        break;
      }
      case "registration_page": {
        this.setRegistrationPage();
        break;
      }
      case "login_page": {
        this.setLoginPage();
        break;
      }
      case "user_page": {
        this.setUserPage();
        break;
      }
      case "trainer_page": {
        this.setTrainerPage();
        break;
      }
      case "user_activities_page": {
        this.setUserActivitiesPage();
        break;
      }
      case "user_account_details_page": {
        this.setUserAccountDetailsPage();
        break;
      }
    }
  }
}
