import {IUser} from "../interface/user.interface";

export class User implements  IUser{

  userId: number;
  name: string;
  lastName: string;
  email: string;
  password: string;
  address: string;
  age: number;

  constructor() {
    this.init();
  }

  init() {
    this.userId=null;
    this.name="";
    this.lastName="";
    this.email="";
    this.password="";
    this.address="";
    this.age=null;
  }

  public static createNewUser(user: User): User {
    let newUser = new User();
    newUser.name = user.name;
    newUser.lastName = user.lastName;
    newUser.email = user.email;
    newUser.password = user.password;
    newUser.address = user.address;
    newUser.age = user.age;
    return newUser;
  }

}
