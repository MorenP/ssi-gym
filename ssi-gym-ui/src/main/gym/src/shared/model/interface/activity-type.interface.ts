export interface ActivityType {
  activityTypeId: number;
  name: string;
  description: string;

  selected: boolean;
}
