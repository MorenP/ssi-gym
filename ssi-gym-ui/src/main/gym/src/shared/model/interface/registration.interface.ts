export interface IRegistration {
  name: string;
  lastName: string;
  email: string;
  password: string;
  repeatPassword: string;
  address: string;
}
