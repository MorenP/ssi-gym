export interface Trainer {
  trainerId: number;
  name: string;
  lastName: string;
  login: string;
  password: string;
  pictureName: string;
  description: string;
}
