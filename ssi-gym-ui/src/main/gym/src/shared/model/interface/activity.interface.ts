import {Trainer} from "./trainer.interface";
import {Hall} from "./hall.interface";
import {ActivityType} from "./activity-type.interface";

export interface Activity {
  activityId: number;
  trainer: Trainer;
  hall: Hall;
  name: string;
  time: string;
  activityType: ActivityType;
}
