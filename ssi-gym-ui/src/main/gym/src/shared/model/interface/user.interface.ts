export interface IUser {
  userId: number;
  name: string;
  lastName: string;
  email: string;
  password: string;
  address: string;
  age: number;
}
