export interface Hall {
  hallId: number;
  name: string;
  amount: number;
}
