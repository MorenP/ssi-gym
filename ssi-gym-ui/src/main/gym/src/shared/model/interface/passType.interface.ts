export interface IPassType {
  passTypeId: number;
  name: string;
  price: number;
  description: string;
}
