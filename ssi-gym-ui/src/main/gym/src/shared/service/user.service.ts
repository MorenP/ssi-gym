import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {environment} from "../../environments/environment";
import {Md5} from "ts-md5/dist/md5";
import {Login} from "../model/class/login.class";
import {Registration} from "../model/class/registration.class";

@Injectable()
export class UserService {

  constructor(private http: HttpClient) {
  }

  getUser(login: Login): Observable<any> {

    let user = new Login();
    user.login = login.login;
    user.password = Md5.hashStr(login.password).toString();

    let headers = new HttpHeaders();
    headers = headers.append("Access-Control-Allow-Headers", "*");
    headers = headers.append('Access-Control-Allow-Origin', '*');
    headers = headers.append('Access-Control-Allow-Methods', 'GET, OPTIONS, POST, PUT');
    headers = headers.append('Content-Type', 'application/json' );

    return this.http.post(environment.apiEndpoint + 'user/login', JSON.stringify(user).toString(), {
      headers: headers})
      .map((res: Response) => res);
  }

  register(registration: Registration): Observable<any> {
    console.log("REGISTER");
    let user = new Registration();
    user.name = registration.name.toString();
    user.lastName = registration.lastName.toString();
    user.address = registration.address.toString();
    user.email = registration.email.toString();
    user.password = Md5.hashStr(registration.password).toString();
    user.repeatPassword = Md5.hashStr(registration.repeatPassword).toString();

    let headers = new HttpHeaders();
    headers = headers.append("Access-Control-Allow-Headers", "*");
    headers = headers.append('Access-Control-Allow-Origin', '*');
    headers = headers.append('Access-Control-Allow-Methods', 'GET, OPTIONS, POST, PUT');
    headers = headers.append('Content-Type', 'application/json' );
    // headers = headers.append('Accept-Charset',  'utf-8, iso-8859-1;q=0.5');
    // headers = headers.append('Accept-Language',  'fr-CH, fr;q=0.9, en;q=0.8, de;q=0.7, *;q=0.5');

    return this.http.post(environment.apiEndpoint + 'user/register', JSON.stringify(user).toString(), {
      headers: headers})
      .map((res: Response) => res);
  }
}
