import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Trainer} from "../model/interface/trainer.interface";

import 'rxjs/Rx';
import {environment} from "../../environments/environment";

@Injectable()
export class TrainersService {

  trainers: Trainer[];

  constructor(private http: HttpClient) {
  }

  getTrainers(): Observable<any> {
    let headers = new HttpHeaders();
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Access-Control-Allow-Headers', 'Content-Type');
    headers.append('Access-Control-Allow-Methods', 'GET, OPTIONS');
    return this.http.get(environment.apiEndpoint+'trainers' ,{headers: headers}).map((res:Response) => res);
  }
}
