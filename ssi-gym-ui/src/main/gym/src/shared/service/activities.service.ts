import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {environment} from "../../environments/environment";

@Injectable()
export class ActivitiesService {

  constructor(private http: HttpClient) {
  }

  getAllActivities(): Observable<any> {
      let headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Origin', '*');
      headers.append('Access-Control-Allow-Headers', 'Content-Type');
      headers.append('Access-Control-Allow-Methods', 'GET, OPTIONS');
      return this.http.get(environment.apiEndpoint + 'activities', {headers: headers}).map((res: Response) => res);
  }

  getActivityTypes(): Observable<any> {
      let headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Origin', '*');
      headers.append('Access-Control-Allow-Headers', 'Content-Type');
      headers.append('Access-Control-Allow-Methods', 'GET, OPTIONS');
      return this.http.get(environment.apiEndpoint + 'activitytypes', {headers: headers}).map((res: Response) => res);
  }
}
