package pk.ssi.gym.dao;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import pk.ssi.gym.model.Activity;
import pk.ssi.gym.util.HibernateUtil;

import java.util.List;

@Repository("activityDao")
public class ActivityDaoImpl implements ActivityDao {
    @Override
    public void create(Activity activity) {

    }

    @Override
    public Activity getActivity() {
        return null;
    }

    @Override
    public List<Activity> getAllActivity() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<Activity> activities = session.createQuery("from Activity ").list();

        session.getTransaction().commit();
        session.close();
        return activities;
    }

    @Override
    public void delete(int id) {

    }
}
