package pk.ssi.gym.dao;

import pk.ssi.gym.model.Activity;
import pk.ssi.gym.model.User;

import java.util.List;

public interface ActivityDao {
    void create(Activity activity);
    Activity getActivity();
    List<Activity> getAllActivity();
    void delete(int id);
}
