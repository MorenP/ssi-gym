package pk.ssi.gym.dao;

import com.google.gson.Gson;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import pk.ssi.gym.model.ActivityType;
import pk.ssi.gym.util.HibernateUtil;

import java.util.List;

@Repository("activityTypeDao")
public class ActivityTypeDaoImpl implements ActivityTypeDao {

    @Override
    public void create(ActivityType activityType) {

    }

    @Override
    public String getActivityType() {
        return null;
    }

    @Override
    public List<ActivityType> getAllAcitivityTypes() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<ActivityType> activityTypes = session.createQuery("from ActivityType").list();

        session.getTransaction().commit();
        session.close();

        return activityTypes;
    }

    @Override
    public void delete(int id) {

    }
}
