package pk.ssi.gym.dao;

import com.google.gson.Gson;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import pk.ssi.gym.model.Trainer;
import pk.ssi.gym.util.HibernateUtil;

import java.util.List;

@Repository("trainerDao")
public class TrainerDaoImpl implements TrainerDao {
    @Override
    public void create(Trainer trainer) {

    }

    @Override
    public Trainer getTrainer(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        Trainer trainer = (Trainer) session.createQuery("from Trainer ").uniqueResult();

        session.getTransaction().commit();
        session.close();

        return trainer;
    }

    @Override
    public List<Trainer> getAllTrainers() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<Trainer> trainers = session.createQuery("from Trainer ").list();

        session.getTransaction().commit();
        session.close();

        System.out.println(new Gson().toJson(trainers));

        return trainers;
    }

    @Override
    public void delete(int id) {

    }
}
