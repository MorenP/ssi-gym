package pk.ssi.gym.dao;

import pk.ssi.gym.model.User;

import java.util.List;

public interface UserDao {
    void create(User user);
    User getUser(String login, String password);
    List<User> getAllUsers();
    void delete(int id);
    int getNextId();
    User getUserByEmail(String email);
}
