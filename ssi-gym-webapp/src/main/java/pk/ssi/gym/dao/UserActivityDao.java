package pk.ssi.gym.dao;

import pk.ssi.gym.model.UserActivity;

import java.util.List;

public interface UserActivityDao {
    void create(UserActivity userActivity);
    UserActivity getUserActivity(int id);
    List<UserActivity> getAllActivities();
    void delete(int id);
}
