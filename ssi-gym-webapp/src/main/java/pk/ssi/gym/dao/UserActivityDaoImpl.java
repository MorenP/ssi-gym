package pk.ssi.gym.dao;

import com.google.gson.Gson;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import pk.ssi.gym.model.UserActivity;
import pk.ssi.gym.util.HibernateUtil;

import java.util.List;

@Repository("userActivityDao")
public class UserActivityDaoImpl implements UserActivityDao {
    @Override
    public void create(UserActivity userActivity) {

    }

    @Override
    public UserActivity getUserActivity(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        UserActivity userActivity = (UserActivity) session
                .createQuery("from UserActivity ua where ua.user.id = id ")
                .uniqueResult();

        session.getTransaction().commit();
        session.close();

        return userActivity;
    }

    @Override
    public List<UserActivity> getAllActivities() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<UserActivity> usersActivities = session.createQuery("from UserActivity ").list();

        session.getTransaction().commit();
        session.close();

        return usersActivities;
    }

    @Override
    public void delete(int id) {

    }
}
