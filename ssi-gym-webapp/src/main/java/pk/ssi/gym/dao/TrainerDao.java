package pk.ssi.gym.dao;

import pk.ssi.gym.model.Trainer;

import java.util.List;

public interface TrainerDao {
    void create(Trainer trainer);
    Trainer getTrainer(int id);
    List<Trainer> getAllTrainers();
    void delete(int id);
}
