package pk.ssi.gym.dao;

import pk.ssi.gym.model.PassType;

import java.util.List;

public interface PassTypeDao {
    void create(PassType passType);
    PassType getPassType(int id);
    List<PassType> getAllPassTypes();
    void delete(int id);
}
