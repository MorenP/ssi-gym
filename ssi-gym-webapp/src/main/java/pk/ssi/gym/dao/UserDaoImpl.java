package pk.ssi.gym.dao;

import com.google.gson.Gson;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import pk.ssi.gym.model.User;
import pk.ssi.gym.util.HibernateUtil;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.Arrays;
import java.util.List;

@Repository("userDao")
public class UserDaoImpl implements UserDao {
    @Override
    public void create(User user) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(user);
        session.getTransaction().commit();
    }

    @Override
    public User getUser(String login, String password) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        String hql = "FROM pk.ssi.gym.model.User u where u.email ='" + login +"' AND u.password ='" + password+"'";
        //List<User> user = session.createQuery(hql).list();
        User newUser = (User) session.createQuery(hql).uniqueResult();
        System.out.println("USER: " + login + " PASS: " + password);
        session.getTransaction().commit();
        session.close();

        return newUser;
    }

    @Override
    public List<User> getAllUsers() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<User> users = session.createQuery("from User ").list();

        session.getTransaction().commit();
        session.close();

        return users;
    }

    @Override
    public void delete(int id) {

    }

    @Override
    public int getNextId() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        int id = Integer.parseInt(session.createQuery("select max(u.userId) from User u").list().get(0).toString());

        session.getTransaction().commit();
        session.close();

        return id;
    }

    @Override
    public User getUserByEmail(String email) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        String hql = "FROM pk.ssi.gym.model.User u where u.email ='" + email +"'";
        //List<User> user = session.createQuery(hql).list();
        User user = (User) session.createQuery(hql).uniqueResult();

            session.getTransaction().commit();
        session.close();

        return user;
    }
}
