package pk.ssi.gym.dao;

import pk.ssi.gym.model.Hall;

import java.util.List;

public interface HallDao {
    void create(Hall hall);
    Hall getHall(int id);
    List<Hall> getAllhalls();
    void delete(int id);
}
