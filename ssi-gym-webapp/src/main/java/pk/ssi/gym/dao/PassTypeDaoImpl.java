package pk.ssi.gym.dao;

import com.google.gson.Gson;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import pk.ssi.gym.model.PassType;
import pk.ssi.gym.util.HibernateUtil;

import java.util.List;

@Repository("passTyoeDao")
public class PassTypeDaoImpl implements PassTypeDao {
    @Override
    public void create(PassType passType) {

    }

    @Override
    public PassType getPassType(int id) {
        return null;
    }

    @Override
    public List<PassType> getAllPassTypes() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<PassType> passTypes = session.createQuery("from PassType").list();

        session.getTransaction().commit();
        session.close();

        return passTypes;
    }

    @Override
    public void delete(int id) {

    }
}
