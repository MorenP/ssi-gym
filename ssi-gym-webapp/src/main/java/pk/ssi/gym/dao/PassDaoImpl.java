package pk.ssi.gym.dao;

import com.google.gson.Gson;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import pk.ssi.gym.model.Pass;
import pk.ssi.gym.model.PassType;
import pk.ssi.gym.util.HibernateUtil;

import java.util.List;

@Repository("passDao")
public class PassDaoImpl implements PassDao {
    @Override
    public void create(Pass pass) {

    }

    @Override
    public Pass getPass(int id) {
        return null;
    }

    @Override
    public List<Pass> getAllPasses() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<Pass> passes = session.createQuery("from Pass").list();

        session.getTransaction().commit();
        session.close();

        return passes;
    }

    @Override
    public void delete(int id) {

    }
}
