package pk.ssi.gym.dao;

import pk.ssi.gym.model.Pass;

import java.util.List;

public interface PassDao {
    void create(Pass pass);
    Pass getPass(int id);
    List<Pass> getAllPasses();
    void delete(int id);
}
