package pk.ssi.gym.dao;

import com.google.gson.Gson;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import pk.ssi.gym.model.Hall;
import pk.ssi.gym.util.HibernateUtil;

import java.util.List;

@Repository("hallDao")
public class HallDaoImpl implements HallDao {
    @Override
    public void create(Hall hall) {

    }

    @Override
    public Hall getHall(int id) {
        return null;
    }

    @Override
    public List<Hall> getAllhalls() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<Hall> halls = session.createQuery("from Hall").list();

        session.getTransaction().commit();
        session.close();

        return halls;
    }

    @Override
    public void delete(int id) {

    }
}
