package pk.ssi.gym.dao;

import pk.ssi.gym.model.ActivityType;

import java.util.List;

public interface ActivityTypeDao {
    void create(ActivityType activityType);
    String getActivityType();
    List<ActivityType> getAllAcitivityTypes();
    void delete(int id);
}
