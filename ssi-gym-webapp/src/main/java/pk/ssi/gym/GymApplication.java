package pk.ssi.gym;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import pk.ssi.gym.model.User;
import pk.ssi.gym.model.UserActivity;
import pk.ssi.gym.service.UserService;

@SpringBootApplication
@EnableAutoConfiguration
public class GymApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(GymApplication.class);
	}

	private static UserService userService;

	@Autowired
	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public static void main(String[] args) {

//		System.out.println("Maven + Hibernate + Oracle");
//		Session session = HibernateUtil.getSessionFactory().openSession();
//
//		session.beginTransaction();
//		DBUser user = new DBUser();
//
//		user.setUserId(101);
//		user.setUsername("superman");
//		user.setCreatedBy("system");
//		user.setCreatedDate(new Date());
//
//		Trenerzy trener = new Trenerzy();
//
//		trener.setId(4);
//		trener.setImie("Adam");
//		trener.setNazwisko("Nowicki");
//		trener.setLogin("anowicki");
//		trener.setHaslo("qwertyuiop");
//		trener.setNazwa_zdjecia("anowicki.jpg");
//
//		// session.save(user);
//		session.save(trener);
//
//		session.getTransaction().commit();


		SpringApplication.run(GymApplication.class, args);
//		System.out.println(userService.getUser("mnowak", "6eea9b7ef19179a06954edd0f6c05ceb"));
		//System.out.println("sout1" + User.getUserJSON("mnowak", "6eea9b7ef19179a06954edd0f6c05ceb"));
		//System.out.println("sout2" + userService.getUser("mnowak", "6eea9b7ef19179a06954edd0f6c05ceb").toString());
		//System.out.println("sout3" + userService.getUserJSON("mnowak", "6eea9b7ef19179a06954edd0f6c05ceb"));
	}
}
