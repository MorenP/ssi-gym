package pk.ssi.gym.service;

import pk.ssi.gym.model.Trainer;

public interface TrainerService {
    void create(Trainer trainer);
    String getTrainerJSON(int id);
    String getAllTrainersJSON();
    void delete(int id);
    void update(Trainer trainer);
}
