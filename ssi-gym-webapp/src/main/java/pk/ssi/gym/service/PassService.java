package pk.ssi.gym.service;

import pk.ssi.gym.model.Pass;

public interface PassService {
    void create(Pass pass);
    String getPassJSON(int id);
    String getAllPassesJSON();
    void delete(int id);
    void update(Pass pass);
}
