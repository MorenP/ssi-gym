package pk.ssi.gym.service;

import pk.ssi.gym.model.Activity;

public interface ActivityService {
    void create(Activity activity);
    String getActivityJSON(int id);
    String getAllActivitiesJSON();
    void delete(int id);
    void update(Activity activity);
}
