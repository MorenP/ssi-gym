package pk.ssi.gym.service;

import pk.ssi.gym.model.User;
import pk.ssi.gym.model.UserRegistration;

import java.util.List;

public interface UserService {
    void create(User user);
    String getUserJSON(String login, String password);
    User getUser(String login, String password);
    String getAllUsersJSON();
    User register(UserRegistration userRegistration);
    void delete(int id);
    void update(User user);
}
