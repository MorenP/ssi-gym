package pk.ssi.gym.service;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pk.ssi.gym.dao.TrainerDao;
import pk.ssi.gym.model.Trainer;

@Service
public class TrainerServiceImpl implements TrainerService {
    private TrainerDao trainerDao;

    @Autowired
    public void setTrainerDao(TrainerDao trainerDao) {
        this.trainerDao = trainerDao;
    }

    @Override
    public void create(Trainer trainer) {

    }

    @Override
    public String getTrainerJSON(int id) {
        return new Gson().toJson(trainerDao.getTrainer(id));
    }

    @Override
    public String getAllTrainersJSON() {
        return new Gson().toJson(trainerDao.getAllTrainers());
    }

    @Override
    public void delete(int id) {

    }

    @Override
    public void update(Trainer trainer) {

    }
}
