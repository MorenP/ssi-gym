package pk.ssi.gym.service;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pk.ssi.gym.dao.UserActivityDao;
import pk.ssi.gym.model.UserActivity;

@Service
public class UserActivityServiceImpl implements UserActivityService {
    private UserActivityDao userActivityDao;

    @Autowired
    public void setUserActivityDao(UserActivityDao userActivityDao) {
        this.userActivityDao = userActivityDao;
    }

    @Override
    public void create(UserActivity userActivity) {

    }

    @Override
    public String getUserActivityJSON(int id) {
        return new Gson().toJson(userActivityDao.getUserActivity(id));
    }

    @Override
    public String getAllUserActivtiesJSON() {
        return new Gson().toJson(userActivityDao.getAllActivities());
    }

    @Override
    public void delete(int id) {

    }

    @Override
    public void update(UserActivity userActivity) {

    }
}
