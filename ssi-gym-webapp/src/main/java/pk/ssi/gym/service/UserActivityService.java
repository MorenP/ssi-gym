package pk.ssi.gym.service;

import pk.ssi.gym.model.UserActivity;

public interface UserActivityService {
    void create(UserActivity userActivity);
    String getUserActivityJSON(int id);
    String getAllUserActivtiesJSON();
    void delete(int id);
    void update(UserActivity userActivity);
}
