package pk.ssi.gym.service;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pk.ssi.gym.dao.ActivityDao;
import pk.ssi.gym.model.Activity;

@Service
public class ActivityServiceImpl implements ActivityService {
    private ActivityDao activityDao;

    @Autowired
    public void setActivityDao(ActivityDao activityDao) {
        this.activityDao = activityDao;
    }

    @Override
    public void create(Activity activity) {

    }

    @Override
    public String getActivityJSON(int id) {
        return new Gson().toJson(activityDao.getActivity());
    }

    @Override
    public String getAllActivitiesJSON() {
        return new Gson().toJson(activityDao.getAllActivity());
    }

    @Override
    public void delete(int id) {

    }

    @Override
    public void update(Activity activity) {

    }
}
