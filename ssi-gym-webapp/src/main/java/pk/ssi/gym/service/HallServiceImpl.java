package pk.ssi.gym.service;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pk.ssi.gym.dao.HallDao;
import pk.ssi.gym.model.Hall;

@Service
public class HallServiceImpl implements HallService {
    private HallDao hallDao;

    @Autowired
    public void setHallDao(HallDao hallDao) {
        this.hallDao = hallDao;
    }

    @Override
    public void create(Hall hall) {

    }

    @Override
    public String getHallJSON(int id) {
        return new Gson().toJson(hallDao.getHall(id));
    }

    @Override
    public String getAllHallsJSON() {
        return new Gson().toJson(hallDao.getAllhalls());
    }

    @Override
    public void delete(int id) {

    }

    @Override
    public void update(Hall hall) {

    }
}
