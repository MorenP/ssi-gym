package pk.ssi.gym.service;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pk.ssi.gym.dao.PassTypeDao;
import pk.ssi.gym.model.PassType;

@Service
public class PassTypeServiceImpl implements PassTypeService {
    private PassTypeDao passTypeDao;

    @Autowired
    public void setPassTypeDao(PassTypeDao passTypeDao) {
        this.passTypeDao = passTypeDao;
    }

    @Override
    public void create(PassType passType) {

    }

    @Override
    public String getPassTypeJSON(int id) {
        return new Gson().toJson(passTypeDao.getPassType(id));
    }

    @Override
    public String getAllPassTypesJSON() {
        return new Gson().toJson(passTypeDao.getAllPassTypes());
    }

    @Override
    public void delete(int id) {

    }

    @Override
    public void update(PassType passType) {

    }
}
