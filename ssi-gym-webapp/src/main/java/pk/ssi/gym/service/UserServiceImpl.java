package pk.ssi.gym.service;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pk.ssi.gym.dao.UserDao;
import pk.ssi.gym.model.User;
import pk.ssi.gym.model.UserRegistration;

import javax.transaction.Transactional;
import java.util.List;

@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {
    private UserDao userDao;

    @Autowired
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public void create(User user) {

    }

    @Override
    public User getUser(String login, String password) {
        return userDao.getUser(login, password);
    }

    @Override
    public String getUserJSON(String login, String password) {
        return new Gson().toJson(userDao.getUser(login, password));
    }

    @Override
    public String getAllUsersJSON() {
        return new Gson().toJson(userDao.getAllUsers());
    }

    @Override
    public User register(UserRegistration userRegistration) {

        int id = userDao.getNextId()+1;

        User tempUser = userDao.getUserByEmail(userRegistration.getEmail());

        if(userRegistration.getPassword().equals(userRegistration.getRepeatPassword()) && tempUser==null) {
            User user = new User();
            user.setUserId(id);
            user.setName(userRegistration.getName());
            user.setLastName(userRegistration.getLastName());
            user.setEmail(userRegistration.getEmail());
            user.setLogin(userRegistration.getName() + userRegistration.getLastName());
            user.setAge(userRegistration.getAge());
            user.setPassword(userRegistration.getPassword());
            // TODO address
            // TODO czy trener
            userDao.create(user);
            return getUser(userRegistration.getEmail(), userRegistration.getPassword());
        } else {
            return null;
        }
    }

    @Override
    public void delete(int id) {
    }

    @Override
    public void update(User user) {
    }
}
