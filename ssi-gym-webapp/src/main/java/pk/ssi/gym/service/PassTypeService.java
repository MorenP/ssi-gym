package pk.ssi.gym.service;

import pk.ssi.gym.model.PassType;

public interface PassTypeService {
    void create(PassType passType);
    String getPassTypeJSON(int id);
    String getAllPassTypesJSON();
    void delete(int id);
    void update(PassType passType);
}
