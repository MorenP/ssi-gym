package pk.ssi.gym.service;

import pk.ssi.gym.model.Hall;

public interface HallService {
    void create(Hall hall);
    String getHallJSON(int id);
    String getAllHallsJSON();
    void delete(int id);
    void update(Hall hall);
}
