package pk.ssi.gym.service;

import pk.ssi.gym.model.ActivityType;

public interface ActivityTypeService {
    void create(ActivityType activitytype);
    String getActivityTypeJSON(int id);
    String getAllActivityTypesJSON();
    void delete(int id);
    void update(ActivityType activityType);
}
