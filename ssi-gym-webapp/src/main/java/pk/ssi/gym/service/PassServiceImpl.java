package pk.ssi.gym.service;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pk.ssi.gym.dao.PassDao;
import pk.ssi.gym.model.Pass;

@Service
public class PassServiceImpl implements PassService {
    private PassDao passDao;

    @Autowired
    public void setPassDao(PassDao passDao) {
        this.passDao = passDao;
    }

    @Override
    public void create(Pass pass) {

    }

    @Override
    public String getPassJSON(int id) {
        return new Gson().toJson(passDao.getPass(id));
    }

    @Override
    public String getAllPassesJSON() {
        return new Gson().toJson(passDao.getAllPasses());
    }

    @Override
    public void delete(int id) {

    }

    @Override
    public void update(Pass pass) {

    }
}
