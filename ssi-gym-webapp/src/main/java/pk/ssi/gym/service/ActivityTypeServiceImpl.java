package pk.ssi.gym.service;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pk.ssi.gym.dao.ActivityTypeDao;
import pk.ssi.gym.model.ActivityType;

@Service
public class ActivityTypeServiceImpl implements ActivityTypeService {
    private ActivityTypeDao activityTypeDao;

    @Autowired
    public void setActivityTypeDao(ActivityTypeDao activityTypeDao) {
        this.activityTypeDao = activityTypeDao;
    }

    @Override
    public void create(ActivityType activitytype) {

    }

    @Override
    public String getActivityTypeJSON(int id) {
        return new Gson().toJson(activityTypeDao.getActivityType());
    }

    @Override
    public String getAllActivityTypesJSON() {
        return new Gson().toJson(activityTypeDao.getAllAcitivityTypes());
    }

    @Override
    public void delete(int id) {

    }

    @Override
    public void update(ActivityType activityType) {

    }
}
