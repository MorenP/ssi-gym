package pk.ssi.gym.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pk.ssi.gym.model.Activity;
import pk.ssi.gym.model.ActivityType;
import pk.ssi.gym.model.UserActivity;
import pk.ssi.gym.service.ActivityService;
import pk.ssi.gym.service.ActivityTypeService;
import pk.ssi.gym.service.UserActivityService;


@RestController
public class ActivityController {
    private ActivityTypeService activityTypeService;
    private UserActivityService userActivityService;
    private ActivityService activityService;

    @Autowired
    public void setActivityTypeService(ActivityTypeService activityTypeService) {
        this.activityTypeService = activityTypeService;
    }

    @Autowired
    public void setUserActivityService(UserActivityService userActivityService) {
        this.userActivityService = userActivityService;
    }

    @Autowired
    public void setActivityService(ActivityService activityService) {
        this.activityService = activityService;
    }

    @RequestMapping(value = "/api/activitytypes", method = RequestMethod.GET)
    public String getAllActivityTypes() {
        return activityTypeService.getAllActivityTypesJSON();
    }

    @RequestMapping(value = "/api/usersactivities", method = RequestMethod.GET)
    public String getAllUsersActivities() {
        return userActivityService.getAllUserActivtiesJSON();
    }

    @RequestMapping(value = "/api/activities", method = RequestMethod.GET)
    public String getAllActivities() {
        Activity.displayActivities();
        return activityService.getAllActivitiesJSON();
    }

}
