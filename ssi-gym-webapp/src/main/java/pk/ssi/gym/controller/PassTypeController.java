package pk.ssi.gym.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pk.ssi.gym.service.PassTypeService;

@RestController
@RequestMapping(value = "api/passTypes")
public class PassTypeController {
    @Autowired
    private PassTypeService passTypeService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String getPriceList() {
        return passTypeService.getAllPassTypesJSON();
    }
}
