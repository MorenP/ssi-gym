package pk.ssi.gym.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pk.ssi.gym.model.Hall;
import pk.ssi.gym.service.HallService;

@RestController
public class HallController {
    private HallService hallService;

    @Autowired
    public void setHallService(HallService hallService) {
        this.hallService = hallService;
    }

    @RequestMapping(value = "api/halls", method = RequestMethod.GET)
    public String getHalls() {
        return hallService.getAllHallsJSON();
    }
}
