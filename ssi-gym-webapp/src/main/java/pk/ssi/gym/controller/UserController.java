package pk.ssi.gym.controller;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pk.ssi.gym.model.User;
import pk.ssi.gym.model.UserLogin;
import pk.ssi.gym.model.UserRegistration;
import pk.ssi.gym.service.UserActivityService;
import pk.ssi.gym.service.UserService;

@RestController
public class UserController {
    private UserService userService;
    private UserActivityService userActivityService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setUserActivityService(UserActivityService userActivityService) {
        this.userActivityService = userActivityService;
    }

    @RequestMapping(value = "/api/user/login", method = RequestMethod.POST, consumes ="application/json", produces="application/json")
    @ResponseBody
    public ResponseEntity<String> getUser(@RequestBody UserLogin userLogin){
        HttpHeaders responseHeaders = new HttpHeaders();
        User user = userService.getUser(userLogin.getLogin(), userLogin.getPassword());
        if (user != null) {
            return new ResponseEntity<>(new Gson().toJson(user), responseHeaders, HttpStatus.OK);
        } else {
            return new ResponseEntity<>("{\"error\": true}", responseHeaders, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/api/user/{id}/activities", method = RequestMethod.GET)
    public String getAllUserActivities(
            @PathVariable("id") int id) {
        return userActivityService.getUserActivityJSON(id);
    }

    @RequestMapping(value = "/api/user/register", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public ResponseEntity<String> registerUser(@RequestBody UserRegistration userRegistration) {

        HttpHeaders responseHeaders = new HttpHeaders();
        User user = userService.register(userRegistration);
        if (user != null) {
            return new ResponseEntity<>(new Gson().toJson(user), responseHeaders, HttpStatus.OK);
        } else {
            return new ResponseEntity<>("{\"error\": true}", responseHeaders, HttpStatus.OK);
        }
    }
}
