package pk.ssi.gym.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pk.ssi.gym.model.Pass;
import pk.ssi.gym.model.PassType;
import pk.ssi.gym.service.PassService;
import pk.ssi.gym.service.PassTypeService;

@RestController
public class PassController {
    private PassService passService;
    private PassTypeService passTypeService;

    @Autowired
    public void setPassService(PassService passService) {
        this.passService = passService;
    }

    @Autowired
    public void setPassTypeService(PassTypeService passTypeService) {
        this.passTypeService = passTypeService;
    }

    @RequestMapping(value = "api/passtypes", method = RequestMethod.GET)
    public String getPassTypes() {
        return passTypeService.getAllPassTypesJSON();
    }

    @RequestMapping(value = "api/passes", method = RequestMethod.GET)
    public String getPasses() {
        return passService.getAllPassesJSON();
    }
}
