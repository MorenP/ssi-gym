package pk.ssi.gym.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pk.ssi.gym.model.Trainer;
import pk.ssi.gym.service.TrainerService;


import javax.servlet.http.HttpServletResponse;

@RestController
public class TrainersController {

    private TrainerService trainerService;

    @Autowired
    public void setTrainerService(TrainerService trainerService) {
        this.trainerService = trainerService;
    }

    @RequestMapping(value = "api/trainers", method = RequestMethod.GET)
    public String greeting(@RequestParam(value="name", defaultValue="World") String name, HttpServletResponse response) {
        response.setHeader("Access-Control-Allow-Origin","*");
        Trainer.displayTrainers();
        return trainerService.getAllTrainersJSON();
    }
}
