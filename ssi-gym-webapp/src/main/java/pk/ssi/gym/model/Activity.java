package pk.ssi.gym.model;

import com.google.gson.Gson;
import org.hibernate.Session;
import pk.ssi.gym.util.HibernateUtil;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

@Entity
@Table(name = "ACTIVITIES")
public class Activity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ACTIVITY_ID", unique = true, nullable = false)
    private int activityId;

    @ManyToOne
    @JoinColumn(name = "TRAINER_ID")
    private Trainer trainer;

    @ManyToOne
    @JoinColumn(name = "HALL_ID")
    private Hall hall;

    @Column(name = "NAME", nullable = false, length = 40)
    private String name;

    @Column(name = "TIME", nullable = false)
    private String time;

    @Column(name = "DESCRIPTION", length = 1500)
    private String description;

    @ManyToOne
    @JoinColumn(name = "ACTIVITY_TYPE_ID")
    private ActivityType activityType;

    @OneToMany(targetEntity = UserActivity.class, mappedBy = "activity", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private transient Collection<UserActivity> userActivities;

    public Activity() {
    }

    public Activity(Trainer trainer, Hall hall, String name, String time, String description, ActivityType activityType) {
        this.trainer = trainer;
        this.hall = hall;
        this.name = name;
        this.time = time;
        this.description = description;
        this.activityType = activityType;
    }

    public int getActivityId() {
        return activityId;
    }

    public void setActivityId(int activityId) {
        this.activityId = activityId;
    }

    public Trainer getTrainer() {
        return trainer;
    }

    public void setTrainer(Trainer trainer) {
        this.trainer = trainer;
    }

    public Hall getHall() {
        return hall;
    }

    public void setHall(Hall hall) {
        this.hall = hall;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ActivityType getActivityType() {
        return activityType;
    }

    public void setActivityType(ActivityType activityType) {
        this.activityType = activityType;
    }

    public Collection<UserActivity> getUserActivities() {
        return userActivities;
    }

    public void setUserActivities(Collection<UserActivity> userActivities) {
        this.userActivities = userActivities;
    }

/*    public static String getActivitiesTypesJSON() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<Activity> activities = session.createQuery("from Activity ").list();

        session.getTransaction().commit();
        session.close();

        return new Gson().toJson(activities);
    }*/

    public static void displayActivities() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<Activity> activities = session.createQuery("from Activity ").list();
        activities.stream().forEach(System.out::println);

        session.getTransaction().commit();
        session.close();
    }
}