package pk.ssi.gym.model;

import com.google.gson.Gson;
import org.hibernate.Session;
import pk.ssi.gym.util.HibernateUtil;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

@Entity
@Table(name="HALL")
public class Hall {

    @Id
    @Column(name = "HALL_ID", unique = true, nullable = false)
    private int hallId;
    @Column(name="NAME", nullable = false)
    private String name;
    @Column(name = "AMOUNT", nullable = false)
    private int amount;
    @Column(name="DESCRIPTION")
    private String description;

    @OneToMany(targetEntity = Activity.class, mappedBy = "hall", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private transient Collection<Activity> activities;

    public Hall() {
    }

    public Hall(int hallId, String name, int amount, String description) {
        this.hallId = hallId;
        this.name = name;
        this.amount = amount;
        this.description = description;
    }

    public int getHallId() {
        return hallId;
    }

    public void setHallId(int hallId) {
        this.hallId = hallId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Collection<Activity> getActivities() {
        return activities;
    }

    public void setActivities(Collection<Activity> activities) {
        this.activities = activities;
    }

    @Override
    public String toString() {
        return "Hall{" +
                "id=" + hallId +
                ", name='" + name + '\'' +
                ", amount=" + amount +
                ", description='" + description + '\'' +
                '}';
    }
/*
    public static String getAllHallsJSON() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<Hall> halls = session.createQuery("from Hall").list();

        session.getTransaction().commit();
        session.close();

        return new Gson().toJson(halls);
    }*/

    public static void displayAllHalls() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<Hall> halls = session.createQuery("from Hall").list();
        halls.stream().forEach(System.out::println);

        session.getTransaction().commit();
        session.close();
    }
}
