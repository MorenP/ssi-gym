package pk.ssi.gym.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserLogin {

    private String login;
    private String password;

    public UserLogin() {
    }

    public UserLogin(String login, String password) {
        this.login = login;
        this.password = password;
    }
    @JsonProperty("login")
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @JsonProperty("password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
