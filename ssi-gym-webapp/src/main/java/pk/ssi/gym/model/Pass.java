package pk.ssi.gym.model;

import com.google.gson.Gson;
import org.hibernate.Session;
import pk.ssi.gym.util.HibernateUtil;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "PASSES")
public class Pass {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PASS_ID", unique = true, nullable = false)
    private int passId;
    @ManyToOne
    @JoinColumn(name = "PASS_TYPE_ID")
    private PassType passType;
    @ManyToOne
    @JoinColumn(name = "USER_ID")
    private User user;
    @Column(name = "START_DATE")
    private String startDate;
    @Column(name = "END_DATE")
    private String endDate;
    @Column(name="STATUS", length = 20)
    private String status;

    public Pass() {
    }

    public Pass(int passId, PassType passType, User user, String startDate, String endDate, String status) {
        this.passId = passId;
        this.passType = passType;
        this.user = user;
        this.startDate = startDate;
        this.endDate = endDate;
        this.status = status;
    }

    public int getPassId() {
        return passId;
    }

    public void setPassId(int passId) {
        this.passId = passId;
    }

    public PassType getPassType() {
        return passType;
    }

    public void setPassType(PassType passType) {
        this.passType = passType;
    }

    public User getUser() {
        return user;
    }

    public void setUserId(User user) {
        this.user = user;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Pass{" +
                "passId=" + passId +
                ", passType='" + passType + '\'' +
                ", userId='" + user + '\'' +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                '}';
    }

/*    public static String getPassesJSON() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<PassType> passTypes = session.createQuery("from Pass").list();

        session.getTransaction().commit();
        session.close();

        return new Gson().toJson(passTypes);
    }*/

    public static void displayPasses() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<PassType> passTypes = session.createQuery("from Pass").list();
        passTypes.stream().forEach(System.out::println);

        session.getTransaction().commit();
        session.close();
    }
}