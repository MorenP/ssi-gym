package pk.ssi.gym.model;

import com.google.gson.Gson;
import org.hibernate.Session;
import pk.ssi.gym.util.HibernateUtil;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "TRAINERS")
public class Trainer implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TRAINER_ID", unique = true, nullable = false)
    private int trainerId;
    @Column(name="NAME", nullable = false, length = 40)
    private String name;
    @Column(name="LAST_NAME", nullable = false, length = 40)
    private String lastName;
    @Column(name="LOGIN", nullable = false, length = 20)
    private String login;
    @Column(name="PASSWORD", nullable = false, length = 20)
    private String password;
    @Column(name="PICTURE_NAME", nullable = false, length = 40)
    private String pictureName;
    @Column(name="DESCRIPTION", nullable = false, length = 100)
    private String description;
    @OneToMany(targetEntity = Activity.class, mappedBy = "trainer", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private transient  Collection<Activity> activities;

    public Trainer() {
    }

    public Trainer(String name, String lastName, String login, String password, String pictureName, String description, Collection<Activity> activities) {
        this.name = name;
        this.lastName = lastName;
        this.login = login;
        this.password = password;
        this.pictureName = pictureName;
        this.description = description;
        this.activities = activities;
    }

    public int getTrainerId() {
        return trainerId;
    }

    public void setTrainerId(int trainerId) {
        this.trainerId = trainerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPictureName() {
        return pictureName;
    }

    public void setPictureName(String pictureName) {
        this.pictureName = pictureName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Collection<Activity> getActivities() {
        return activities;
    }

    public void setActivities(Collection<Activity> activities) {
        this.activities = activities;
    }

    public static List<Trainer> getTrainers() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<Trainer> trainers = session.createQuery("from Trainer ").list();

        session.getTransaction().commit();
        session.close();

        return trainers;
    }

    public static void displayTrainers() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<Trainer> trainers = session.createQuery("from Trainer ").list();
        trainers.stream().forEach(System.out::println);

        session.getTransaction().commit();
        session.close();
    }

/*    public static String getTrainersJSON() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<Trainer> trainers = session.createQuery("from Trainer ").list();

        session.getTransaction().commit();
        session.close();

        System.out.println(new Gson().toJson(trainers));

        return new Gson().toJson(trainers);
    }*/

    @Override
    public String toString() {
        return "Trainer{" +
                "id=" + trainerId +
                ", name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", pictureName='" + pictureName + '\'' +
                ", description='" + description + '\'' +
                ", activities=" + activities +
                '}';
    }
}