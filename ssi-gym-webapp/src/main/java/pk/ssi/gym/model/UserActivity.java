package pk.ssi.gym.model;

import com.google.gson.Gson;
import org.hibernate.Session;
import pk.ssi.gym.util.HibernateUtil;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "USERS_ACTIVITIES")
public class UserActivity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "USER_ACTIVITY_ID", unique = true, nullable = false)
    private int userActivityId;
    @ManyToOne
    @JoinColumn(name = "ACTIVITY_ID")
    private Activity activity;
    @ManyToOne
    @JoinColumn(name = "USER_ID")
    private User user;

    public UserActivity() {
    }

    public UserActivity(int userActivityId, Activity activity, User user) {
        this.userActivityId = userActivityId;
        this.activity = activity;
        this.user = user;
    }

    public int getUserActivityId() {
        return userActivityId;
    }

    public void setUserActivityId(int userActivityId) {
        this.userActivityId = userActivityId;
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

   /* public static String getUsersActivitiesJSON() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<UserActivity> usersActivities = session.createQuery("from UserActivity ").list();

        session.getTransaction().commit();
        session.close();

        return new Gson().toJson(usersActivities);
    }

    public static String getUserActivitiesJSON(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<UserActivity> usersActivities = session.createQuery("from UserActivity ua where ua.userActivityId = id ").list();

        session.getTransaction().commit();
        session.close();

        return new Gson().toJson(usersActivities);
    }*/

    public static void displayUsersActivities() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<UserActivity> usersActivities = session.createQuery("from UserActivity ").list();
        usersActivities.stream().forEach(System.out::println);

        session.getTransaction().commit();
        session.close();
    }
}