package pk.ssi.gym.model;

import com.google.gson.Gson;
import org.hibernate.Session;
import pk.ssi.gym.util.HibernateUtil;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

@Entity
@Table(name = "PASS_TYPE")
public class PassType {

    @Id
    @Column(name = "PASS_TYPE_ID", unique = true, nullable = false)
    private int passTypeId;
    @Column(name = "NAME", nullable = false)
    private String name;
    @Column(name = "PRICE", nullable = false, length = 5, precision = 2)
    private double price;
    @Column(name = "DESCRIPTION")
    private String description;

    @OneToMany(targetEntity = Pass.class, mappedBy = "passType", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private transient Collection<Pass> passes;
    public PassType() {
    }

    public PassType(int passTypeId, String name, double price, String description) {
        this.passTypeId = passTypeId;
        this.name = name;
        this.price = price;
        this.description = description;
    }

    public int getPassTypeId() {
        return passTypeId;
    }

    public void setPassTypeId(int passTypeId) {
        this.passTypeId = passTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Collection<Pass> getPasses() {
        return passes;
    }

    public void setPasses(Collection<Pass> passes) {
        this.passes = passes;
    }

    @Override
    public String toString() {
        return "PassType{" +
                "id=" + passTypeId +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", description='" + description + '\'' +
                '}';
    }

/*    public static String getPassTypesJSON() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<PassType> passTypes = session.createQuery("from PassType").list();

        session.getTransaction().commit();
        session.close();

        return new Gson().toJson(passTypes);
    }*/

    public static void displayPassTypesTypes() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<PassType> passTypes = session.createQuery("from PassType").list();
        passTypes.stream().forEach(System.out::println);

        session.getTransaction().commit();
        session.close();
    }


}
