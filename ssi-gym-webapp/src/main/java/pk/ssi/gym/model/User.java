package pk.ssi.gym.model;

import com.google.gson.Gson;
import org.hibernate.Session;
import pk.ssi.gym.util.HibernateUtil;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

@Entity
@Table(name="USERS")
public class User {

    @Id
    @Column(name = "USER_ID", unique = true, nullable = false)
    private int userId;
    @Column(name="NAME", nullable = false, length = 40)
    private String name;
    @Column(name="LAST_NAME", nullable = false, length = 40)
    private String lastName;
    @Column(name="LOGIN", nullable = false, length = 20)
    private String login;
    @Column(name="PASSWORD", nullable = false, length = 20)
    private String password;
    @Column(name="EMAIL", nullable = false, length = 40)
    private String email;
    @Column(name="AGE", nullable = false, length = 40)
    private int age;

    @OneToMany(targetEntity = Pass.class, mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private transient Collection<Pass> passes;
    @OneToMany(targetEntity = UserActivity.class, mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private transient Collection<UserActivity> userActivities;

    public User() {
    }

    public User(int userId, String name, String lastName, String login, String password, String email, int age) {
        this.userId = userId;
        this.name = name;
        this.lastName = lastName;
        this.login = login;
        this.password = password;
        this.email = email;
        this.age = age;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Collection<Pass> getPasses() {
        return passes;
    }

    public void setPasses(Collection<Pass> passes) {
        this.passes = passes;
    }

    public Collection<UserActivity> getUserActivities() {
        return userActivities;
    }

    public void setUserActivities(Collection<UserActivity> userActivities) {
        this.userActivities = userActivities;
    }

    public static void displayTrainers() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<User> users = session.createQuery("from User ").list();
        users.stream().forEach(System.out::println);

        session.getTransaction().commit();
        session.close();
    }

/*    public static String getAllUsersJSON() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<User> users = session.createQuery("from User ").list();

        session.getTransaction().commit();
        session.close();

        return new Gson().toJson(users);
    }*/

/*    public static String getUserJSON(String login, String password) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        String hql = "FROM pk.ssi.gym.model.User u where u.login ='" + login +"' AND u.password ='" + password+"'";
        List<User> user = session.createQuery(hql).list();

        session.getTransaction().commit();
        session.close();

        return new Gson().toJson(user);
    }*/

    @Override
    public String toString() {
        return "User{" +
                "id=" + userId +
                ", name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", age=" + age +
                '}';
    }
}
