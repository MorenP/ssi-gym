package pk.ssi.gym.model;

import com.google.gson.Gson;
import org.hibernate.Session;
import pk.ssi.gym.util.HibernateUtil;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

@Entity
@Table(name = "ACTIVITY_TYPE")
public class ActivityType implements Serializable{

    @Id
    @Column(name = "ACTIVITY_TYPE_ID", unique = true, nullable = false)
    private int activityTypeId;
    @Column(name = "NAME", nullable = false, length = 40)
    private String name;
    @Column(name = "DESCRIPTION", length = 1500)
    private String description;

    @OneToMany(targetEntity = Activity.class, mappedBy = "activityType", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private transient  Collection<Activity> activities;

    public ActivityType() {
    }

    public ActivityType(int activityTypeId, String name, String description) {
        this.activityTypeId = activityTypeId;
        this.name = name;
        this.description = description;
    }

    public int getActivityTypeId() {
        return activityTypeId;
    }

    public void setActivityTypeId(int id) {
        this.activityTypeId = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Collection<Activity> getActivities() {
        return activities;
    }

    public void setActivities(Collection<Activity> activities) {
        this.activities = activities;
    }


/*
    public static String getAllActivityTypesJSON() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<ActivityType> activityTypes = session.createQuery("from ActivityType").list();

        session.getTransaction().commit();
        session.close();

        return new Gson().toJson(activityTypes);
    }*/

    public static void displayActivityTypes() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<ActivityType> activityTypes = session.createQuery("from ActivityType").list();
        activityTypes.stream().forEach(System.out::println);

        session.getTransaction().commit();
        session.close();
    }
}
